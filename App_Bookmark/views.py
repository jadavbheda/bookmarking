# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
#from App_Bookmark.api import UserResource
from django.shortcuts import render    
import json

import sys
import shutil
import urllib2
import lxml.html
import views
import os
from django.conf import settings
import base64
from urlparse import urlparse
from BeautifulSoup import BeautifulSoup
from models import Default_Category_User
import PythonMagick
import traceback
import datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import make_aware, get_current_timezone 

HEADERS = {
    'User-Agent': 'urllib2 (Python %s)' % sys.version.split()[0],
    'Connection': 'close',
}

    
def signinup(request):
	return render(request, 'signin_up.html');
def index(request, username):
    #c = Default_Category_User.objects.get(user=request.user.id)
    expiry_time = datetime.datetime.now()
    expiry_time =  expiry_time + relativedelta(years=1)
    expiry_time = make_aware(expiry_time, get_current_timezone())
    request.session.set_expiry(expiry_time)
    
    try:
        
        default_cat =  request.user.default_user.all()[0]
    except:
        return HttpResponseRedirect('/')
    
    return render(request, 'home.html', {'username': username, 'default_cat' : default_cat.category_id} )
    '''
    data_user = {
    "first_name": "Prashant",
    "last_login": "2013-11-21T12:46:55",
    "last_name": "S",
    "username": "Prashant"
    }
    #response = requests.post('http://localhost:8000/api/v1/user/?format=json',
    #                     data=json.dumps(data_user),
    #                     headers={'content-type': 'application/json'})
    data_link = {
    "user": "/api/v1/user/4/",
    "category": "/api/v1/category/5/",
    "domain_name": "https://google.com/tutorial04/",
    "last_accessed": "2013-11-21T23:23:38",
    "title": "urlPrashant",
    "url": "https://google.com",
    "view_count": 1
    }
    response = requests.post('http://localhost:8000/prashant/api/v1/link/?format=json',
                         data=json.dumps(data_link),
                         headers={'content-type': 'application/json'})
    data_cat = {
    "chrono_order": 1,
    "name": "prashantCategoy",
    "user": "/api/v1/user/4/",
    "link": [],
    }
    
    #response = requests.post('http://localhost:8000/scott/api/v1/category/?format=json',
    #                     data=json.dumps(data_cat),
    #                     headers={'content-type': 'application/json'})
    return HttpResponse()
    #'''

def get_favicon(url, path='static\favicon\favicon.ico', alt_icon_path='static\favicon\alticon.ico'):

    if not url.endswith('/'):
        url += '/'
    
    path = os.path.dirname(__file__)
    path = os.path.join(path, "static")
    path_base = os.path.join(path, "favicon")
    url_file = urlparse(url).netloc
    path = os.path.join(path_base, url_file + ".ico")
    png_path = os.path.join(path_base, url_file + ".png")
    alt_path = os.path.join(path_base, "alticon.ico")
    #print path
    #print favicon_get_url + url
    
    #request = urllib2.Request(favicon_get_url)
    icon = None;
    icon = urllib2.urlopen(url)
    soup = BeautifulSoup(icon)
    icon_url = None;
    
    icon_link = soup.find("link", rel="shortcut icon")
    
    if not icon_link:
        icon_link = soup.find("link", rel="icon")
        
    #meta_link = soup.find("meta", itemprop="image")
    
    if icon_link:
        print "inside icon link"
        
        if icon_link['href'].startswith('.'):
            icon_url = icon_link['href'][1:]
        else:
            icon_url = icon_link['href']
            
        if not icon_url.startswith('http'):
            icon_url = url + icon_url
            
        icon = urllib2.urlopen(icon_url)
        
        with open(path, "wb") as f:
            f.write(icon.read())
        
        
        
    else:
        print "Inside else"
        icon_url = url + "/favicon.ico"
        try:
            icon = urllib2.urlopen(icon_url)
            with open(path, "wb") as f:
                f.write(icon.read())
        except:
            shutil.copyfile(alt_path, path)
    
    try:        
        print type(str(path))
        img = PythonMagick.Image(str(path))
        img.magick('png')
        img.backgroundColor('none')
        img.write(str(png_path))
    
    except:
        print traceback.format_exc()
            
    '''elif meta_link:
        print "inside meta link"
        print meta_link
        
        if meta_link['content'].startswith('.'):
            icon_url = meta_link['content'][1:]
            
        if not meta_link['content'].startswith('http'):
            icon_url = meta_link['content']
            
        icon = urllib2.urlopen(icon_url)
        
        with open(path, "wb") as f:
            f.write(icon.read())
        return'''
        
         
    
    print path
    
    
    '''try:
        icon = urllib2.urlopen(request).read()
        print icon
    except(urllib2.HTTPError, urllib2.URLError):
        reqest = urllib2.Request(url, headers=HEADERS)
        try:
            content = urllib2.urlopen(request).read()
        except(urllib2.HTTPError, urllib2.URLError):
            alt_icon_path = os.path.join(path_base, "alticon.ico")
            shutil.copyfile(alt_icon_path, path)
            return
        icon_path = lxml.html.fromstring(x).xpath(
            '//link[@rel="icon" or @rel="shortcut icon"]/@href'
        )
        if icon_path:
            request = urllib2.Request(url + icon_path[:1], headers=HEADERS)
            try:
                icon = urllib2.urlopen(request).read()
            except(urllib2.HTTPError, urllib2.URLError):
                shutil.copyfile(alt_icon_path, path)
                return'''
    
    
    #print icon
    #print icon.decode('base64')
    
    #icon = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAh9JREFUOI2Nk0FIlFEQx3/z3qe7kGiIoWAdUtBatiDLQD1UngQDhQ6lEuSlc4fw2rFz1wQjomKPVrIdgiK6FIsgiaAiHbYlpZCkRDf3m+nwfZ9tm4cGHrz3Zub//897M1I0AzhkUCdQBtIGOCJTQOK9wI7B7jFJbiAAxGBqZXGx70Q2+2KhUBjDzOMiCFXFe08qnXZdmcw9gUdUWQDwY2urc2Vpqac7m134tLra67z3IoKZoWGImVFXX09XJtNGjQUATgSJVBdGxsZuC3j7IxuD5Pj2QACcQ0QcsCSQO1pVY9EMgdPAdYMGqwFwCQtxUm2ARA/Z9XJ29jbQW+sP9pOs1sVfgKaKHOCPSlBFzbCYMZZ9xOCnwA5ApVL5R91+CWqGxgzxEoMpYDIJrOztHagg6RcsYk1kGzD3cX5+0mAA0DAMTeE4MFQ06yya1RfNWvYBkjeokvnmVE/Pg7Xl5TsCHaoaftvYaH+dzz8E7gInDZocYI1NTTOj4+O3FEoASasKzHR0dy98KZX6VZWW1tYPF4aG8s9yucsGZ4C1IGZLA30O+hXks1kyA7a7vZ1ua28vmaoI7IpI/le5fC2sVC7WBcHjBODck+npmyKCiEQ9YYaZ4b0vXxoeniWaqzbg1cDg4NzzXG5kdGLiqcTTeOXr+vpV573iHC6ORpVUKrXX0NhY+L65ef5wc/N74D5wNgzDG977dwlA8oP/Y1q1l9+NAuJPuXU50AAAAABJRU5ErkJggg=="
    
    #f = open(path, 'w+')
    #f.write(base64.decodestring(icon))
    #f.close()
    
    #print os.path.abspath(f.name)
    
