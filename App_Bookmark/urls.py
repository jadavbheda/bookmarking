from django.conf.urls import patterns, url, include
#from django.conf.urls.defaults import *
from App_Bookmark import views
from tastypie.api import Api
#from App_Bookmark.api import CategoryResource, LinkResource, TagResource, Link_Category_ChronoResource
from App_Bookmark.api import UserResource, CategoryResource, LinkResource, TagResource, Link_Category_ChronoResource

v1_api = Api(api_name='v1')
v1_api.register(UserResource())
v1_api.register(CategoryResource())
v1_api.register(LinkResource())
v1_api.register(TagResource())
v1_api.register(Link_Category_ChronoResource())

urlpatterns = patterns('',
    # The normal jazz here...
    url(r'^$', views.signinup, name="signinup"),
    url(r'^(?P<username>\w+)/$', views.index, name='index'),
    url(r'^(?P<username>\w+)/api/', include(v1_api.urls)),
    url(r'^api/', include(v1_api.urls)),
    )

entry_resource = LinkResource()
'''
urlpatterns = patterns('',
    url(r'^api/', include(entry_resource.urls)),
    url(r'^(?P<username>\w+)/$', views.index, name='index'),
    
    
    # ex: /polls/5/
    #url(r'^(?P<question_id>\d+)/$', views.detail, name='detail'),
    # ex: /polls/5/results/
    #url(r'^(?P<question_id>\d+)/results/$', views.results, name='results'),
    # ex: /polls/5/vote/
    #url(r'^(?P<question_id>\d+)/vote/$', views.vote, name='vote'),
)
'''
