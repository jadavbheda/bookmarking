from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from actions import actionurls, action
from django.http import HttpResponse
from models import Link

#from django.conf.urls import patterns, url, include
#from tastypie.utils import trailing_slash

from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource

from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.authentication import BasicAuthentication

from App_Bookmark.models import Category, Link, Tag, Link_Category_Chrono
import views
import json
import random

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        #excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser', 'date_joined']
        fields = ['username', 'first_name', 'last_name', 'last_login']
        #allowed_methods = ['get']
        filtering = {
            'username': ALL,
        }
        #authentication = BasicAuthentication()
        authorization= Authorization()

class LinkResource(ModelResource):
    user = fields.ForeignKey('App_Bookmark.api.UserResource', 'user', related_name='link')
    category = fields.ForeignKey('App_Bookmark.api.CategoryResource', 'category', related_name='link')
    #category = fields.ForeignKey(CategoryResource, 'category')
    class Meta:
        always_return_data = True
        queryset = Link.objects.all()
        #queryset = Link.objects.select_related('user', 'category')
        resource_name = 'link'
        authorization= Authorization()

        filtering = {
            'user': ALL_WITH_RELATIONS,
            'category': ALL_WITH_RELATIONS,
            #'view_count': ['exact', 'lt', 'lte', 'gte', 'gt'],
        }
        
    def hydrate(self, bundle):
        # Don't change existing slugs.
        # In reality, this would be better implemented at the ``Note.save``
        # level, but is for demonstration.
            #if not bundle.obj.pk:
             #   bundle.obj.slug = slugify(bundle.data['title'])
        
        if bundle.request.method == "POST":
            #print bundle.data['category']
            default_cat =  bundle.request.user.default_user.all()[0]
            #print bundle.request.user.id
            #print default_cat.category_id
            try:
                chrono_order = Link.objects.filter(user_id = bundle.request.user.id, category_id = default_cat.category_id).order_by('-chrono_order')[0].chrono_order + 1
            except:
                chrono_order = 0.0
            bundle.data['chrono_order'] = chrono_order
            
        if 'favicon_dropped' in bundle.data.keys():
            views.get_favicon(bundle.data['domain_name'], self.id)
        else:
            print "nope"
           
        
        return bundle


    #'''
    def dispatch(self, request_type, request, **kwargs):
        username = kwargs.pop('username')
        kwargs['user'] = get_object_or_404(User, username=username)
        return super(LinkResource, self).dispatch(request_type, request, **kwargs)
    #'''
   
    def prepend_urls(self):
       return actionurls(self)
   
    @action(name='changechrono', allowed=['get'], static=False)
    def change_chrono(self, request, **kwargs):
        json_data = []
        for x in request.GET: 
            json_data = json.loads(x)
        
        same_category = json_data['same_category']
        
        
            
        link_id = json_data['link_id']
        new_index = json_data['index']
        lower_index = json_data['lower']
        upper_index = json_data['upper']
        
        #print json_data
        
        #print new_index
        #print upper_index 
        link_obj = Link.objects.get(id=link_id)
        
        print upper_index
        print lower_index
        
        if upper_index and lower_index:
            
            link_previous = Link.objects.get(id=upper_index)
            print link_previous.chrono_order
            
            link_next = Link.objects.get(id=lower_index)
            print link_next.chrono_order
            
            new_chrono_order = random.uniform(link_previous.chrono_order, link_next.chrono_order)
            print new_chrono_order
            
            link_obj.chrono_order = new_chrono_order
            
        
        if ( not upper_index ) and lower_index:
            link_next = Link.objects.get(id=lower_index)
            new_chrono_order = random.uniform(0.0, link_next.chrono_order)
            
            link_obj.chrono_order = new_chrono_order
            
        
        if ( not lower_index ) and upper_index:
            link_previous = Link.objects.get(id=upper_index)
            new_chrono_order = random.uniform(link_previous.chrono_order, link_previous.chrono_order + 1 )
            
            link_obj.chrono_order = new_chrono_order
            
            print link_obj.category_id
            print link_obj.chrono_order
        
        if not lower_index and not upper_index:
            new_chrono_order = 1.0
            link_obj.chrono_order = new_chrono_order
            
        if not same_category:
            link_obj.category_id = json_data['category']
            
        link_obj.save()
        return HttpResponse("Okay")
   
        
class CategoryResource(ModelResource):
    #user = fields.ForeignKey(UserResource, 'user', full=True)
    user = fields.ForeignKey('App_Bookmark.api.UserResource', 'user', related_name='category')
    link = fields.ToManyField('App_Bookmark.api.LinkResource', 'link_set', related_name='category', full=True)
    #choices = fields.ToManyField('polls.api.ChoiceResource', 'choice_set', related_name='poll', full=True) 
 
    class Meta:
        always_return_data = True
        queryset = Category.objects.all()
        resource_name = 'category'
        authorization= Authorization()
        ordering = ['chrono_order']

        filtering = {
            'user': ALL_WITH_RELATIONS,
            #'link': ALL_WITH_RELATIONS,
            #'view_count': ['exact', 'lt', 'lte', 'gte', 'gt'],
        }
    #'''
    def dispatch(self, request_type, request, **kwargs):
        username = kwargs.pop('username')
        kwargs['user'] = get_object_or_404(User, username=username)
        return super(CategoryResource, self).dispatch(request_type, request, **kwargs)
    #''' 

    @action(name='change_cat_chrono', allowed=['get'], static=False)
    def change_cat_chrono(self, request, **kwargs):
        for x in request.GET:
            temp = json.loads(x)
        lower_index = temp['lower_sibling']
        upper_index = temp['upper_sibling']
        cat_id = temp['cat_id']
        
        cat_obj = Category.objects.get(id=cat_id);
        #print cat_obj.chrono_order
        
        if upper_index and lower_index:
            
            cat_previous = Category.objects.get(id=upper_index)
            print cat_previous.chrono_order
            
            cat_next = Category.objects.get(id=lower_index)
            print cat_next.chrono_order
            
            new_chrono_order = random.uniform(cat_previous.chrono_order, cat_next.chrono_order)
            print new_chrono_order
            
            cat_obj.chrono_order = new_chrono_order
            
        
        if ( not upper_index ) and lower_index:
            cat_next = Link.objects.get(id=lower_index)
            new_chrono_order = random.uniform(0.0, cat_next.chrono_order)
            
            cat_obj.chrono_order = new_chrono_order
            
        
        if ( not lower_index ) and upper_index:
            cat_previous = Category.objects.get(id=upper_index)
            new_chrono_order = cat_previous.chrono_order + 1
            
            print new_chrono_order
            
            cat_obj.chrono_order = new_chrono_order
            
            #print cat_obj.category_id
            #print cat_obj.chrono_order
        
        if not lower_index and not upper_index:
            new_chrono_order = 1.0
            cat_obj.chrono_order = new_chrono_order
        
        cat_obj.save()
        
        return HttpResponse("chronological order of categories chenged.")
    
    def prepend_urls(self):
       return actionurls(self)


    def hydrate(self, bundle):
         # Don't change existing slugs.
         # In reality, this would be better implemented at the ``Note.save``
         # level, but is for demonstration.
             #if not bundle.obj.pk:
              #   bundle.obj.slug = slugify(bundle.data['title'])

        if bundle.request.method == "POST":
            print "Now i will update chrono_order"
            chrono_order = Category.objects.filter(user_id = bundle.request.user.id).order_by('-chrono_order')[0].chrono_order + 1
            bundle.data['chrono_order'] = chrono_order
            bundle.data['name'] = "New Category"
        #print max_chrono

        return bundle


class TagResource(ModelResource):
    #user = fields.ForeignKey(UserResource, 'user')
    user = fields.ForeignKey('App_Bookmark.api.UserResource', 'user', related_name='tag')
    link = fields.ForeignKey(LinkResource, 'link')
    class Meta:
        queryset = Tag.objects.all()
        resource_name = 'tag'
        authorization= Authorization()
        filtering = {
            'user': ALL_WITH_RELATIONS
            #'view_count': ['exact', 'lt', 'lte', 'gte', 'gt'],
        }
    
class Link_Category_ChronoResource(ModelResource):
    #user = fields.ForeignKey(UserResource, 'user')
    user = fields.ForeignKey('App_Bookmark.api.UserResource', 'user', related_name='link_category_chrono')
    link = fields.ForeignKey(LinkResource, 'link')
    category = fields.ForeignKey(CategoryResource, 'category')

    class Meta:
        queryset = Link_Category_Chrono.objects.all().select_related('user', 'link','category')

        filtering = {
            'user': ALL_WITH_RELATIONS,
            'link': ALL_WITH_RELATIONS,
            'category': ALL_WITH_RELATIONS,
        }

    def gravatar(self, email):
        pass
