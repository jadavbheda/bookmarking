from django.contrib import admin
from App_Bookmark.models import Category, Link, Tag, Link_Category_Chrono


class LinkInline(admin.TabularInline):
    model = Link
    extra = 3


class CategoryAdmin(admin.ModelAdmin):
    inlines = [LinkInline]

admin.site.register(Category, CategoryAdmin)

#admin.site.register(Category)
#admin.site.register(Link)