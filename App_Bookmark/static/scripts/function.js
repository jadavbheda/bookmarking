/***********user content dynamic*************/
// userid is

var userdata = JSON.stringify({
	"user": API_NAME + "user/" + USER_ID + '/',
});

/***********Global variables used for this script***********/
var start_id;
var flag = false;
var name = '';
var cat_new_response_id = '';
var cat_new_name = '';
var cat_name = '';

/**********************************************************/
LINK_URL = HOST_NAME + USER_NAME + API_NAME + LINK;
CATEGORY_URL = HOST_NAME + USER_NAME + API_NAME + CATEGORY;

/***************************************************************/
function stripTrailingSlash(str) {
    if(str.substr(-1) == '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
}

/***************document ready start*****************************/
$(document).ready(function(){


	var Environment = {
	    //mobile or desktop compatible event name, to be used with '.on' function
	    TOUCH_DOWN_EVENT_NAME: 'mousedown touchstart',
	    TOUCH_UP_EVENT_NAME: 'mouseup touchend',
	    TOUCH_MOVE_EVENT_NAME: 'mousemove touchmove',
	    TOUCH_DOUBLE_TAB_EVENT_NAME: 'dblclick dbltap',
	
	    isAndroid: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    isBlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    isIOS: function() {
	        return navigator.userAgent.match(/iPhone|iPod/i);
	    },
	    isOpera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    isWindows: function() {
	        return navigator.userAgent.match(/IEMobile|ZuneWP7/i);
	    },
	    isMobile: function() {
	        return (Environment.isAndroid() || Environment.isBlackBerry() || Environment.isIOS() || Environment.isOpera() || Environment.isWindows());
	    }
	};	
	
	//alert(Environment.isMobile());
	
	if ( Environment.isMobile() ){



		href = "<link rel='stylesheet' type='text/css' href='" + HOST_NAME + "static/css/mobile.css ' />"

		//alert(href);

		$("head").append(decodeURIComponent(href));

	}

	$('#edit-mode').tooltip();

	/*----------------------------------------------------------------------------------------*/
	$( document ).on( "click", "button.link-delete", function(e){
		//alert("something");
		e.preventDefault();
		var link_id = $(this).attr('data-linkid').replace('link', '');
		$.ajax({
			url: LINK_URL + link_id + "/" + FORMAT,
			type: 'delete',
			contentType: 'application/json',
			dataType: 'json',
			processData: false,
			success: function(data){
				$('.alink').popover('hide');
				$('#link'+link_id).remove();

			}
		});
	});

	/*----------------------------------------------------------------------------------------*/
	$( document ).on( "click", "button.linkedit", function(e) {
		e.preventDefault();
		var link_id = $(this).attr('data-linkid').replace('link', '');

		$.ajax({
			url:  LINK_URL + link_id + "/" + FORMAT,
			type: 'PATCH',
			contentType: 'application/json',
			data: 	JSON.stringify({
						"url": $('#url'+link_id).val(),
						"title" : $('#title'+link_id).val(),
					}),
			dataType: 'json',
			processData: false,
			success: function(data){
				$('.alink').popover('hide');
				$('#link'+link_id).find('a').html($('#title'+link_id).val());
				$('#link'+link_id).find('a').attr('href', $('#url'+link_id).val());
			}
		});
	});

	/*----------------------------------------------------------------------------------------*/
	 $(document).keyup(function(e)
	 {
		 if (e.keyCode == 27)
		 {
			 if($('#edit-mode').hasClass('edit-on'))
			 {
		        $('#edit-mode').css('color','#3276b1');
	  			$('#edit-mode').removeClass('edit-on');
				$('#edit-mode').addClass('edit-off');
				$('.alink>a, .text-muted').removeClass('editon_hover');
				$('.alink').popover('hide');
				$(".alink").unbind('popover');
				//to enable link
				$(".alink").bind('click');

				$('.alink').on('click', function(e){
					if( !$('#edit-mode').hasClass('edit-on') ){
						window.location.href = $(this).children('a').attr('href');
					}
				});

				if( $('.bootstrap-admin-box-title').hasClass('cat-edit-on') )
				{
					name = $('.cat-edit-on').find('.cat-title-val').val();
					temp = $('.bootstrap-admin-box-title.cat-edit-on');
					temp.toggleClass('cat-edit-off');
					temp.toggleClass('cat-edit-on');
					temp.html(cat_name);
				}
			}
		}

		 if (e.keyCode == 13)
		 {
			 if($('#edit-mode').hasClass('edit-on'))
			 {
				 $('.cat-done').trigger('click');
		     }
		}

	});

	/*----------------------------------------------------------------------------------------*/
	$(document).mouseup(function (e)
	{
	    var container = $(".popover");

	    if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
			if($(".popover").is(':visible')){
				//alert("1");
				$('.alink').popover('hide');
				$('.popover').css('display','none');
			}
		}

		container = $('.cat_edit_box');
		if (!container.is(e.target) // if the target of the click isn't the container...
		&& container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
			if($(".cat_edit_box").is(':visible')){
				//cat_title = container.children('.cat-title-val').val();
				container.closest('.bootstrap-admin-box-title').toggleClass('cat-edit-off');
				container.closest('.bootstrap-admin-box-title').toggleClass('cat-edit-on');
				container.closest('.bootstrap-admin-box-title').html(cat_name);
			}
		}
	});

	/*----------------------------------------------------------------------------------------*/
	//to load div links content
	$.ajax({
		url:  CATEGORY_URL,
		method: 'get',
		contentType: 'application/json',
		data: userdata,
		dataType: 'json',
		success:
			//<!--success start
			function ( data ) {

			/*----------------------------------------------------------------------------------------*/
				for(i=0; i< data.objects.length ; i++){

					$('#select_category').append('<option data-uri="'+ data.objects[i].resource_uri   +'" id="'+ data.objects[i].id  +'">'+ data.objects[i].name +'</option>');
					$('#category_wrapper').append($('#cat_content').clone().attr('id', "cat"+data.objects[i].id).attr('data-uri', data.objects[i].resource_uri ).addClass('category'));
					if(data.objects[i].chrono_order == 1){
						$('#cat'+data.objects[i].id).addClass('uncategorized');
					}
					else{
						$('#cat'+data.objects[i].id).addClass('categorized');
					}
					content = $('#' + "cat"+data.objects[i].id ).find('.bootstrap-admin-panel-content');
					content.attr('data-catid', "cat" + data.objects[i].id );
					content.append('<div class="text-muted bootstrap-admin-box-title cat-edit-off">'+ data.objects[i].name +'</div>');
					////alert(escape(data.objects[i].resource_uri));
					for(j=0; j<	data.objects[i].link.length; j++){

						link_temp = data.objects[i].link[j]
						link_url[link_temp.url] = link_temp.id

						if (link_temp.is_in_quickbar){
							quick_link_url[link_temp.url] = link_temp.id
							temp_favicon = $('#favicon_wrapper > li').clone();
							link_uri = new URI(link_temp.domain_name);
							
							if (link_uri.subdomain()){
								uri_favicon = link_uri.subdomain() + "." + link_uri.domain();	
							}
							else{
								uri_favicon = link_uri.domain();
							}
							 
							console.log(uri_favicon); 
							temp_favicon.find('img').attr('src', '/static/favicon/' +uri_favicon+ '.png')
							temp_favicon.find('a').attr('href', link_temp.url)
							temp_favicon.attr('data-linkid', link_temp.id)

							$('#icon-list').append(temp_favicon);
						}

						else {

							content.append($('#link_content').clone().attr('id', 'link' + link_temp.id ).attr('data-domain', link_temp.domain_name ));
							$('#link'+ link_temp.id).find('a').attr('href', link_temp.url ).html(link_temp.title);

						}
					}
					$('#' + "cat"+data.objects[i].id ).find('.bootstrap-admin-box-title').html(data.objects[i].name);

				}
				$('#category_wrapper').append('<div class="" id="add-category-div" style="display: none;"><div id="add-category"><div style="margin:5px 0 0 5%;"><img src="'+iconpath+'/plus.png" id="cat-add-btn"></img><span style="font:15px arial,serif;vertical-align:0px;margin-left:15px;font-style:italic;">Drag link here to put in new category</span></div></div></div>');
				$('#add-category-div').append('<div id="del-category"><div style="margin:0px 0 0 5%;"><img src="'+iconpath+'/trash.png" id="cat-del-btn"></img><span style="font:15px arial,serif;vertical-align:0px;margin-left:15px;font-style:italic;">Drag link/category here to delete</span></div></div>');

				$('#icon-list').find('img').error(function(){

					if (!Environment.isWindows()){

						if(!navigator.userAgent.match(/MSIE/i)){

							$(this).attr('src', '/static/favicon/alticon.png');
							//alert($(this).attr('src'));
						}

					}

					else{

					}

				});

				//alert(( !Environment.isMobile() && window.innerWidth > 768 ));

				if ( !Environment.isMobile() && window.innerWidth > 768 ){
						$('.bootstrap-admin-panel-content').addClass('dontsplit');
						$('#category_wrapper').columnize({columns: 3, lastNeverTallest : true, buildOnce : true});
				}
			/*----------------------------------------------------------------------------------------*/

			$('#category_wrapper').on('mouseenter', '.alink', function(){
				url = $(this).find('a').attr('href');
				$(this).attr('title', url);
			}).on('mouseleave','.alink',function(){
				$(this).removeAttr('title');
			});


			/*----------------------------------------------------------------------------------------*/
			if ( !Environment.isMobile() && window.innerWidth > 768 ){

				//alert("bav moti screen chhe..");

/*
			if( $('.category').length > 2)
			{
				for(i=3; i < $('.category').length; i++)
				{
					cur_element = $('.category').eq(i);
					upper = $('.category').eq( i - 3 );
					//alert(upper.index()-1 +" - "+ i);
					margin_minus = ( cur_element.offset().top - upper.offset().top )- upper.height();
					alert(cur_element.offset().left+"----"+upper.offset().left+"----"+upper.height());
					cur_element.offset({ left: upper.offset().left});
					cur_element.css('marginTop', -(margin_minus));
				}

				var initial_top 	= $('.category').eq(0).offset().top;
				var initial_left 	= $('.category').eq(0).offset().left;
				var first_top 		= $('.category').eq(1).offset().top;
			}
*/
			}
			/*----------------------------------------------------------------------------------------*/
				$('#edit-mode').on('click',function()
				{
					var execute_flag=false;
					$(this).toggleClass('edit-on');
					$(this).toggleClass('edit-off');

					if($(this).hasClass('edit-on'))
					{
						$(this).css('background-color','#ed9c28');
						$('.alink>a, .text-muted').addClass('editon_hover');
						/***********popover***************/
						function alink_popover()
						{
							$(document).on('click', '.alink', function(e)
							{
								e.preventDefault();
								$('.alink').popover({
									animation: true,
									html : true,
									trigger: "click",
									placement: 'auto',
									container: 'body',
									stayOnHover: true,
									delay: {show: 0,hide: 100},
									content: function() {
										if($('#edit-mode').hasClass('edit-on')){
											var id = $(this).attr('id').replace('link', '');
											$.ajax({
												url: LINK_URL + id + "/",
												type: 'GET',
												async: false,
												contentType: 'application/json',
												data: JSON.stringify({
															"user": API_NAME + "user/" + USER_ID + '/',
															"category": $(this).parent().parent().attr('id'),
															"url": $(this).siblings('a').val(),
														}),
												dataType: 'json',
												processData: false,
												success: function(data){
													$('#title').attr('value',data.title);
													$('#url').attr('value',data.url);
													$('.linkedit').attr('data-linkid', id);
													$('.link-delete').attr('data-linkid', id);
												}
											});
											execute_flag=true;
											popover_content = $('#popover_content_wrapper').clone();
											popover_content.find('#title').attr('id', 'title'+id);
											popover_content.find('#url').attr('id', 'url'+id);
											return popover_content.html();
										}
									}
								});
								if(!execute_flag)
									$(this).popover('show');
							});
						}
						alink_popover();
						/***************popover end****************/
					}
					else
					{
						$(this).css('background-color','#3276b1');
						$('.alink>a, .text-muted').removeClass('editon_hover');
						$('.alink').popover('hide');
						$('.alink').unbind('popover');
						//to enable link
						$('.alink').bind('click');

						$('.alink').on('click', function(e){
							if( !$('#edit-mode').hasClass('edit-on') ){
								window.location.href = $(this).children('a').attr('href');
							}
						});

						if( $('.bootstrap-admin-box-title').hasClass('cat-edit-on') )
						{
							temp = $('.bootstrap-admin-box-title.cat-edit-on');
							temp.toggleClass('cat-edit-off');
							temp.toggleClass('cat-edit-on');
							temp.html(name);
						}
					}

				});

			/*----------------------------------------------------------------------------------------*/
				$(document).on('click', '.cat-edit-off',function(){
					if( $('#edit-mode').hasClass('edit-on') && !$('.bootstrap-admin-box-title').hasClass('cat-edit-on')){
						cat_name = $(this).html();
						$(this).toggleClass('cat-edit-off');
						$(this).toggleClass('cat-edit-on');
						$(this).html('<div class="cat_edit_box"><input type="text" value="'+cat_name+'" name="cat-title-inp" class="form-control cat-title-val" /><img src="'+iconpath+'/check.png" class="cat-done web-icon"></img><img src="'+iconpath+'/minus.png" class="web-icon cat-cancel "></img></div>');
						$('.cat-title-val').click(function(){
							this.focus();
							//this.select();
						});
					}
				});

				$(document).on('click', '.cat-done',function(){
					var cat_id = $(this).closest('.category').attr('id').replace("cat","");
					var cat_title = $(this).siblings('.cat-title-val').val();

					$.ajax({
						url: CATEGORY_URL + cat_id + "/" ,
						type: 'PATCH',
						dataType: 'json',
						processData: false,
						async: false,
						contentType: 'application/json',
						data: 	JSON.stringify({
									"name": cat_title,
								}),
						success: function(){
							flag = true;
							console.log('category title success');

						}
					});
					if(flag == true){
						$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-off');
						$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-on');
						$(this).closest('.bootstrap-admin-box-title').html(cat_title);

					}
					else{
						$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-off');
						$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-on');
						$(this).closest('.bootstrap-admin-box-title').html(cat_name);
						console.log('That might be a problem on server side');
					}
				});

				$(document).on('click', '.cat-cancel',function(){
					$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-off');
					$(this).closest('.bootstrap-admin-box-title').toggleClass('cat-edit-on');
					$(this).closest('.bootstrap-admin-box-title').html(cat_name);
				});

			/*----------------------------------------------------------------------------------------*/
				var form = $('#urlform');

				form.on('submit', function(e){

					cat_uri = $('#cat'+ default_category).attr('data-uri');
					cat_id = $('#select_category option:selected').attr('id');
					//selected_category = $('#select_category').val();

					e.preventDefault();
					/* TO DO - Query string should pass the valid url criteria */
			  		if(/^(?:(http|https|ftp):\/\/)?(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/.test($('#newurl').val())){
			      	//if(/^(?:(ftp|http|https):\/\/)?(?:[\w-]+\.)+[a-z]{2,6}\/?$/.test($('#newurl').val())){
						console.log($('#newurl').val());
						$('#newurl').popover('hide');
						$('#newurl').css('border-color','#66afe9');
						$('#newurl').css('outline','0');
						$('#newurl').css('-webkit-box-shadow', 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9');
						$('#newurl').css('box-shadow','inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9');
						
						//$('#newurl.form-control').css({
							//'border-color': '#66afe9',
							//'outline': '0',
							//'-webkit-box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9',
							//'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9'
						//});
						
						//alert("inside if2");
					}
				    else
				    {
			          //alert("Invalid URL");
			          //$("#error").fadeIn("slow");
					  //$("#error").fadeOut("slow");
					  //$('#newurl').tooltip('show');
						//$('#newurl').css('border-color','#CF0007','-webkit-box-shadow', 'inset 0 1px 1px rgba(0,0,0,0.075)', 'box-shadow', 'inset 0 1px 1px rgba(0,0,0,0.075)')
						$('#newurl').popover({
							content: 'Invalid Url',
							trigger: 'manual',
							placement:'bottom',
							html: true,
						}).popover('show');

						clickedAway = false
						isVisible = true
						$('#newurl.form-control:focus').css({
							'border-color': 'red',
							'outline': '0',
							'-webkit-box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px red',
							'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px red'
						});

						$("#newurl").on('click', function() {
							$('#newurl').popover('hide');
							$('#newurl').css('border-color','#66afe9');
							$('#newurl.form-control').css({
								'border-color': '#66afe9',
								'outline': '0',
								'-webkit-box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9',
								'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9'
							});
						});

						$("#newurl").focusout(function(){
							$('#newurl').popover('hide');
							$('#newurl').css('border-color','#66afe9');
							$('#newurl.form-control').css({
								'border-color': '#66afe9',
								'outline': '0',
								'-webkit-box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9',
								'box-shadow': 'inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px #66afe9'
							});
						});
						return ;
					}

					var newurl = $('#newurl').val();
					uri = new URI(newurl);
					if(!uri.protocol()){
						uri.protocol("http");
					}
					uri = new URI(uri.href());
					//alert(uri.domain());
					//alert(uri.subdomain());
					//alert(uri.tld());

					temp = uri.subdomain().split('.')[0]//to filter out www or anything
					
					temp_domain = uri.subdomain().split('.')[0]
					
					if ( temp.indexOf('www') > -1 ) {
						temp = uri.subdomain().replace(temp, "").replace(".", "");
					}
					
					else{

						temp = uri.subdomain();
					}
					
					
					//alert("temp2" + temp);
					
					if ( temp )
						temp = temp + ".";
						
					uri_title = temp + uri.domain().replace(uri.subdomain(), "");
					if (temp_domain){
						temp_domain = temp_domain + ".";
					}
					uri_domain = uri.protocol() + "://" +uri_title + "/"
					//alert(uri_domain);
					
					if (link_url[uri.href()] || link_url[stripTrailingSlash(uri.href())])
					{
						alert("Entered url already exit");
						return ;
					}

				    var data_link_new = JSON.stringify({
						"user": API_NAME + "user/" + USER_ID + "/",
						"category": cat_uri,
						"domain_name": uri_domain,
						"title": uri_title,
						"last_accessed" : new Date(),
						"url": uri.href(),
						"view_count": 1,
					});
					
					//alert(uri_title);
					//alert(uri.protocol() + "://" + uri.domain() + "/");
					//return;
					
					$.ajax({
						url:  LINK_URL,
						type: "post",
						contentType: 'application/json',
						data: data_link_new,
						dataType: 'json',
						processData: false,
						success : function(data){
							form.trigger('reset');
							//alert($("#\\/api\\/v1\\/category\\/10\\/").html())

							content = $('#cat'+ default_category).find('.bootstrap-admin-panel-content');

							//alert(content.html());
							link_content = $('#link_content').clone();
							link_content.attr('id', "link" + data.id );
							link_content.attr('data-domain', data.domain_name );
							link_content.find('a').attr('href', data.url );
							link_content.find('a').html(data.title);

							///alert(link_content)

							content.append(link_content);
							if($('#edit-mode').hasClass('edit-on')){
								alink_popover();
							}
							//$('#link'+ data.id).find('a').attr('href', data.url ).html(data.url);
							link_url[data.url] = data.id;

							$("#link"+data.id).css({'background-color': 'rgba(255,255,0)','border-radius':'4px'});
							setTimeout(function(){
								$("#link"+data.id).animate({
									"background-color": "transparent",
									"border-radius":"0px",
								},200);
							},2000);
						},
						error: function(e) {
							console.log(e)
						}
					});
				});
				//console.log(link_url.keys());

			/*----------------------------------------------------------------------------------------*/
				$('.favicon-link').draggable({

					revert : 'invalid',

				});

		/*----------------------------------------------------------------------------------------*/
    drp();

		function drp(){
				$('.category').droppable({

					items: "#favicon_wrapper > li",
					drop : function(event, ui){

						if(!ui.draggable.hasClass('favicon-link')){

							link_id = ui.draggable.attr('id').replace("link", "");

						}

						catid=$(this).attr('data-uri');

						cat=$(this).attr('id');

						if(ui.draggable.hasClass('favicon-link')) {

						link_id = ui.draggable.parent().attr('data-linkid').replace("link", "");
						ui.draggable.remove();

						$.ajax({
							url:  LINK_URL + link_id + "/",
							type: 'PATCH',
							contentType: 'application/json',
							data: 	JSON.stringify({
										"user": API_NAME+ "user/"+ USER_ID + '/',
										"is_in_quickbar": false,
										"category": catid,
									}),
							dataType: 'json',
							processData: false,
							success: function(data){
								//alert('favicon updated');
								//$(this).append();
								//alert(cat);
								content = $('#' + cat).find('.bootstrap-admin-panel-content');
								//alert(content.html());
								link_content = $('#link_content').clone();
								link_content.attr('id', "link" + data.id )
								link_content.attr('data-domain', data.domain_name )
								link_content.find('a').attr('href', data.url )
								link_content.find('a').html(data.title)

								//alert(link_content)

								content.append(link_content);
								//$('#link'+ data.id).find('a').attr('href', data.url ).html(data.url);
								link_url[data.url] = data.id;
                                quick_link_url[data.url] = null;
							}
						})

					}
				}

			});

}
			/*----------------------------------------------------------------------------------------*/
				$('#favicon_bar').droppable({
					items: ".alink",
					drop : function(event, ui){
						//alert(ui.draggable.find("a").attr('id'));

						ui.draggable.remove();

						link_id =  ui.draggable.attr('id').replace("link", "");
						//alert(link_id);
						var a= quick_link_url[ui.draggable.find('a').attr('href')];

						if(a){
							alert("The link is already added in the quick access bar");
							return;
						}
						temp_favicon = $('#favicon_wrapper > li').clone();
						temp_favicon.find("a").attr('href', ui.draggable.attr('data-domain'));
						favicon_link = ui.draggable.attr('data-domain') + "favicon.ico"

						temp_favicon.find('img').error(function(){

							$(this).attr('src', '/static/favicon/alticon.png');
							//alert($(this).attr('src'));

						});

						temp_favicon.find('img').attr('src', favicon_link );
						temp_favicon.attr('data-linkid', link_id);
						$('#icon-list').append(temp_favicon);

						quick_link_url[ui.draggable.find('a').attr('href')] = link_id;

						$('.favicon-link').draggable({

							revert : 'invalid',

						});

						//alert(API_NAME + LINK + link_id + FORMAT)

						$.ajax({
							url:  LINK_URL + link_id + "/" + FORMAT,
							type: 'PATCH',
							contentType: 'application/json',
							data: 	JSON.stringify({
										"user": API_NAME+ "user/"+ USER_ID + '/',
										"is_in_quickbar": API_NAME+ "user/"+ USER_ID + '/',
										"favicon_dropped" : true,
									}),
							dataType: 'json',
							processData: false,
							success: function(data){
								//alert('favicon updated');
							}
						})
					},
				});

			/*----------------------------------------------------------------------------------------*/
				var source_category;

				$(".bootstrap-admin-panel-content > link").draggable({
					revert : 'invalid',

				});

				function makesortable(){


					$(".bootstrap-admin-panel-content").sortable({
						connectWith: ".bootstrap-admin-panel-content , .icons-attach",
						revert : false,
						helper: "clone",
						items : '.alink',
						scroll : true,
						scrollSensitivity: 10,

						start : function(event, ui){
						/* $('.column > *').unwrap(); */
						//when drag is start
							$('#add-category-div').show();
							$('#add-category-div').css('opacity','1.16');
							source_category = ui.item.parent();
							source_category_id = source_category.attr('data-catid').replace("cat", "");
						},

						stop : function(event, ui){

						},

						update : function(event, ui){
							//when there is change occured
							  $('#add-category-div').hide();
							//check if the item is dropped in same category or a different one
							var link_id = ui.item.attr('id').replace("link", "");
							var cat_id = ui.item.parent().attr('data-catid').replace("cat", "");

							s = ui.item.siblings();

							try {
								upper_sibling = ui.item.prev().attr('id').replace("link", "");
							}
							catch(e){
								upper_sibling = null;
							}

							try{
								lower_sibling = ui.item.next().attr('id').replace("link", "");
							}
							catch(e){
								lower_sibling = null;

							}

							if (this === ui.item.parent()[0]){
								if (ui.sender == null){
									//alert(ui.item.parent().attr('data-catid'));
									console.log(ui.item.index());


									$.ajax({
										url: LINK_URL + link_id + "/changechrono/",
										type : 'get',
										contentType: 'application/json',
										data: 	JSON.stringify({
													// TODO - What is the need of USER ? (Only category should be suffice)
													//"user": API_NAME+ "user/"+ USER_ID + '/',
													//"category": span_link.parent().attr('id'),
													"same_category" : true,
													"index" : ui.item.index(),
													"link_id" : link_id,
													"upper" : upper_sibling,
													"lower" : lower_sibling,
												}),
										dataType: 'json',
										processData: false,
										success: function(data){
											//link update
										}
									});
									return
								}
								else
								{
									$.ajax({
										url: LINK_URL + link_id + "/changechrono/",
										type : 'get',
										contentType: 'application/json',
										data: 	JSON.stringify({
													"same_category" : false,
													"category": cat_id,
													"index" : ui.item.index(),
													"link_id" : link_id,
													"upper" : upper_sibling,
													"lower" : lower_sibling,
												}),
										dataType: 'json',
										processData: false,
										success: function(data){
											//$('.alink').popover('hide');
										}
									});

								}
							}
						},

						receive : function(event, ui){
							// i don't know
						}

					}).disableSelection();
				}

				delcategory();
				addcategory();
				makesortable();

				//$('.column > *').unwrap();






			/*----------------------------------------------------------------------------------------*/
			function makeCategorySortable(){

					//	alert("SOmething");

					$('#category_wrapper').sortable({
						connectWith: "#category_wrapper",
						items : ".categorized",
						revert: true,
						handle : '.text-muted',
						tolerance: 'pointer',
						scroll : true,
						scrollSensitivity: 10,
						appendTo: document.body,
						cursor: "move",
						cursorAt: {left: 35,top: 10},

						start : function(event, ui){
							//when drag is start
							/* $('#category_wrapper').columnize({width: 400}); */
							 delcategory();
							 addcategory();
							 drp();
							 $('#add-category-div').show();
							 $('#add-category-div').css('opacity','1.16');
							first_index = ui.item.index();
							first_cat_id = ui.item.attr('id').replace("cat","");
						},

						stop : function(event, ui){



						},
						change: function(event,ui){
								/* $('.column > *').unwrap(); */

							/*  var droppableId = $(this).child().attr("id");
							 alert(droppableId); */
							/*  var arr = $('#category_wrapper > div').get();
							 alert(arr); */

							last_index = ui.placeholder.index();
							last_cat = $('.category').eq(last_index-1);

							console.log(last_index);
						},
						update: function(event, ui){

							var cat_id = ui.item.attr('id');
							next = ui.item.next()
							prev = ui.item.prev()

							cats = $('.category');
							upper_sibling = [];
							lower_sibling = [];

							for (i=0;i<cats.length;i++) {


							console.log(cats.eq(i).find('.bootstrap-admin-box-title').html());

							if (cats.eq(i).attr('id') == cat_id){

								try {
									upper_sibling = cats.eq(i-1).attr('id').replace('cat', '');
									up_name = cats.eq(i-1).find('.bootstrap-admin-box-title').html();
								}

								catch(e){
									upper_sibling = null;
								}

								try {
									lower_sibling = cats.eq(i+1).attr('id').replace('cat', '');
								}

								catch(e){

									lower_sibling = null;
								}

							}


						}

							cat_url = CATEGORY_URL + ui.item.attr('id').replace("cat", "") + "/";
							var cat_id = cat_id.replace("cat", "");

							//alert(upper_sibling);
							//alert(lower_sibling);

							/*

							//alert(cat_id);

							s = ui.item.siblings();

							//alert(s);
							try {
								upper_sibling = ui.item.prev().attr('id').replace("cat", "");
							}
							catch(e){
								upper_sibling = null;
							}

							try{
								if(ui.item.next().hasClass('categorized')){
									lower_sibling = ui.item.next().attr('id').replace("cat", "");
								}

								else{

									lower_sibling = null
								}
							}
							catch(e){
								lower_sibling = null;

							} */

							$.ajax({
								url : cat_url+"change_cat_chrono/",
								type: 'GET',
								contentType : 'application/json',
								data : 	JSON.stringify({
											"cat_id" : cat_id,
											"lower_sibling": lower_sibling,
											"upper_sibling": upper_sibling,
										}),
								dataType: 'json',
								processData: true,
								success: function(data){
									//link update
								}
							});



							  $('#add-category-div').hide();
							  $('#add-category').droppable('destroy');
							  $('.category').droppable('destroy'); 
							  $('#del-category').droppable('destroy');
	  
	  
							$('.bootstrap-admin-panel-content').sortable('destroy');
							$('.bootstrap-admin-panel-content').addClass('dontsplit');
							$('.column > *').unwrap(); 
							$('#category_wrapper').columnize({columns: 3, lastNeverTallest : true, buildOnce : true});
							makesortable();
							addcategory();
							delcategory();
							drp();	
							/* drp(); */
							
	/* $('#add-category').hide();c	
	$('#del-category').hide(); */
						//////to make
							/* $('.category').eq(0).offset({ top: initial_top , left: initial_left });
							if( $('.category').length==3)
							{
								for(i=1; i<3; i++)
								{
									cur_element = $('.category').eq(i);
									prev = $('.category').eq(i-1);
									cur_element.offset({ top: first_top, left: prev.offset().left+prev.width() });
								}
							}
							if( $('.category').length > 3)
							{
								for(i=3; i < $('.category').length; i++)
								{
									cur_element = $('.category').eq(i);
									upper = $('.category').eq( i - 3 );
									cur_element.offset({ top: upper.offset().top+upper.height(), left: upper.offset().left });
								}
							} */
						///////
						},
					});
			
			}
			
			makeCategorySortable();

			/*----------------------------------------------------------------------------------------*/
						/*----------------------------------------------------------------------------------------*/
		/* addcategory();
		$('#add-category').droppable('destroy');
		$('.bootstrap-admin-panel-content').addClass('dontsplit'); 
		$('#category_wrapper').columnize({columns: 3}); */
		/* addcategory(); */
		
		
		function addcategory(){	
			$('#add-category').droppable({
					accept: '.alink, .favicon-link',

					activate: function(event, ui){
						$('#add-category-div').fadeIn(700);
						$('#add-category-div').css('opacity','1.16');
					},

					drop: function(event, ui){

						$.ajax({
							url: CATEGORY_URL,
							type: 'post',
							contentType: 'application/json',
							data: 	JSON.stringify({
										"user": API_NAME + "user/" + USER_ID + "/",
										"link": [],

									}),
							dataType: 'json',
							processData: true,
							async: false,
							success: function(data){
								cat_new_response_id = data.id;
								cat_new_name = data.name;
								//alert(data.name);
							}
						});
						
						var link_id = null;
						
						if(ui.draggable.hasClass('favicon-link')){
							
							link_id = ui.draggable.parent().attr('data-linkid');
						}
						
						else{
							
							link_id = ui.draggable.attr('id').replace("link","");
						
						}
						
						var new_link;
						
						$.ajax({
							url:  LINK_URL + link_id + "/",
							type: 'PATCH',
							contentType: 'application/json',
							data: 	JSON.stringify({
										"user": API_NAME + "user/" + USER_ID + '/',
										"category": API_NAME + CATEGORY + cat_new_response_id + "/",
										"is_in_quickbar" : false,
									}),
							dataType: 'json',
							processData: false,
							async: false,
							success: function(data){
								new_link = data;
							}
						});

						$('#add-category-div').before(
							$('#cat_content').clone().attr('id', 'cat'+cat_new_response_id).attr('data-uri', '/api/v1/category/'+cat_new_response_id+'/' ).addClass('category ui-sortable ui-droppable').fadeIn(1000).addClass('categorized')
						);
						
						content = $("#cat"+cat_new_response_id).find('.bootstrap-admin-panel-content');
						content.attr('data-catid', "cat"+cat_new_response_id );
						content.append('<div class="text-muted bootstrap-admin-box-title cat-edit-off">'+cat_new_name+'</div>');

						//alert(link_content);
						
						if(ui.draggable.hasClass('favicon-link')){
								
								//alert(new_link);
								link_content = $('#link_content').clone();
								link_content.attr('id', "link" + new_link.id )
								link_content.attr('data-domain', new_link.domain_name )
								link_content.find('a').attr('href', new_link.url )
								link_content.find('a').html(new_link.title)
								content.append(link_content);
						
						}
						
						else {
							
							content.append(ui.draggable.clone().css('display','inline-block'));
						
						}
						ui.draggable.remove();
						//window.location.reload();
						makesortable();
						$('#add-category-div').fadeOut(700);
						$('#add-category-div').css('opacity','0');
						//alert("Refreshing");
						$('#category_wrapper').sortable('destroy');
						//alert($('#category_wrapper').find('.category').eq($('#category_wrapper').find('.category').length - 1).attr('id'));
						makeCategorySortable(); 	
						
						 
					},

					deactivate: function(event, ui){
						$('#add-category-div').fadeOut(700);
						$('#add-category-div').css('opacity','0');

					}

				});

			}	/*----------------------------------------------------------------------------------------*/
		function delcategory(){
			$('#del-category').droppable({
					accept: '.alink, .category, .favicon-link',

					activate: function(event, ui){
						$('#add-category-div').fadeIn(700);
					},

					drop: function(event, ui){
					
						if(ui.draggable.hasClass('favicon-link')){
						
						var id = ui.draggable.parent().attr('data-linkid');
						
						$.ajax({
								url: LINK_URL + id + "/",
								type: 'delete',
								
							});
						}
						
						else if (ui.draggable.hasClass('alink'))
						{
							var id = ui.draggable.attr('id').replace("link","");;
							var resource_uri = ui.draggable.attr('data-uri');
							$.ajax({
								url: LINK_URL + id + "/",
								type: 'delete',
								
							});
							var url = ui.draggable.children('a').attr('href');
							delete link_url[url];
						}
						else if (ui.draggable.hasClass('category'))
						{
						    /* $('.column > *').unwrap(); */ 
							var id = ui.draggable.attr('id').replace("cat","");
							var resource_uri = ui.draggable.attr('data-uri');
							$.ajax({
								url: CATEGORY_URL + id + "/",
								type: 'delete',
							});
						}

						ui.draggable.remove();

						$('#add-category-div').fadeOut(700);
					},

					deactivate: function(event, ui){
						$('#add-category-div').fadeOut(700);
						/* $('.column > *').unwrap();
						$('#category_wrapper').columnize({columns: 3}); */
					}

				});
			}
			/*----------------------------------------------------------------------------------------*/
			if( Environment.isMobile() )
			{
				$("#category_wrapper").sortable("disable");
				$('.bootstrap-admin-panel-content').sortable("disable");
				$('.favicon-link').draggable("disable")
			}
			$(window).on("resize",function(){
				if( window.innerWidth <= 768 ){
					$("#category_wrapper").sortable("disable");
					$('.bootstrap-admin-panel-content').sortable("disable");
					$('.link').css('background-color','transparent');
				}
				else{
					/*makeCategorySortable();
					makesortable();
					addcategory();
					delcategory();
					drp();*/
					$("#category_wrapper").sortable("enable");
					$('.bootstrap-admin-panel-content').sortable("enable");
					/*$("#category_wrapper").sortable("refresh");
					$('.bootstrap-admin-panel-content').sortable("refresh");
					$('.column > *').unwrap();
					$('.bootstrap-admin-panel-content').addClass('dontsplit');
					$('#category_wrapper').columnize({columns: 3, lastNeverTallest : true, buildOnce: true});*/
					$('.link').css('background-color','transparent');
				}
			});
			$('.category').eq(0).find('.text-muted').mouseenter(function(){
				$(this).css('cursor','context-menu');
			});

		}
		//-->end success
	});
	//-->ajax completed


});
//end document ready function
