#from tastypie.utils.timezone import now
from django.contrib.auth.models import User
from django.db import models
from urlparse import urlparse
from userena.signals import signup_complete

class Category(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    chrono_order = models.FloatField(default=0.0)
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['chrono_order']

class Link(models.Model):
    
    user = models.ForeignKey(User)
    category = models.ForeignKey(Category)
    
    url = models.URLField(max_length=1024)
    domain_name = models.URLField(max_length=200)
    title = models.CharField(max_length=100)
    last_accessed = models.DateTimeField('Last Accessed')
    view_count = models.PositiveSmallIntegerField()
    is_in_quickbar = models.BooleanField(default=False)
    chrono_order = models.FloatField(default=0.0)
    
    class Meta:
        ordering = ['chrono_order']
    
    def __unicode__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        url_obj = urlparse(self.url)
        
        if not url_obj.scheme:
            self.url = 'http://' + self.url
            
        super(Link, self).save(*args, **kwargs)

class Tag(models.Model):
    user = models.ForeignKey(User)
    link = models.ForeignKey(Link)
    name = models.CharField(max_length=50)
    def __unicode__(self):
        return self.name
    

    
class Link_Category_Chrono(models.Model): 
    user = models.ForeignKey(User)
    link = models.ForeignKey(Link)
    category = models.ForeignKey(Category)
    chrono_order = models.PositiveSmallIntegerField()
    
class Default_Category_User(models.Model):
    user = models.ForeignKey(User, related_name="default_user")
    category = models.ForeignKey(Category)    
    
def createDefaultCat(user , **kwargs):
    print "Creating Default Category for user"
    c = Category()
    c.name = "New"
    c.user = user
    c.chrono_order = 1
    c.save()
    
    cate_default = Default_Category_User()
    cate_default.user = user
    cate_default.category = c
    
    cate_default.save()
    
    
    
signup_complete.connect(createDefaultCat, dispatch_uid="something")
'''
#TODO - folder chrono can be maintained in the folder table itself or Folder_Category_Chrono (need discussion)    
class Folder(models.Model):
    user = models.ForeignKey(User)
    
    parent_folder = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=100)

#TODO - folder chrono can be maintained in the folder table itself or below table (need discussion)
class Folder_Category_Chrono(models.Model): 
    user = models.ForeignKey(User)
    folder = models.ForeignKey(Folder)
    category = models.ForeignKey(Category)
    chrono_order = models.PositiveSmallIntegerField()    
'''    
